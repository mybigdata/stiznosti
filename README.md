# Stížnostní systém

This system makes complaints accessible to all, without any requirement for prior experience or legal education.

## Goal 1 - achieved
Generate one type of complaint based on user-defined inputs.

## Goal 2 - achieved
Support multiple types of complaints and let the user choose.

## Goal 3 - in progress
GUI Builder is required for Rapid Application Development.
Different forms need to be defined and maintained visually __with no code or low code__.  