﻿var laws = [
	{
		key: '2004_500', val: 'Správní řád č. 500/2004 Sb.', paragraphs:
			[
				['spravni_rad/42', 'Podnět podle § 42 správního řádu'],
				['spravni_rad/175', '§ 175 stížnost'],
				['spravni_rad/jiny', 'Jiný podnět (bude zařazen úřadem)'],
			]
	},
	{
		key: '2009_40', val: 'Trestní zákoník č. 40/2009 Sb.', paragraphs:
			[
				['trest/329', '§ 329 zneužití pravomoci úřední osoby'],
			]
	},
	{
		key: '1999_106', val: 'Zákon o svobodném přístupu k informacím č. 106/1999 Sb.', paragraphs:
			[
				['informace/13', '§ 13 žádost o poskytnutí informace'],
			]
	}
];

function getCurrentDate() {
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    return day + ". " + month + ". " + year;
}

function copyToClipboard(element) {
    var clone = $("#clone");
    clone.empty();
    clone.append($(element).clone());
    $.each($(element).find("input[type=text]"), function (i, e) {
        var id = $(e).attr("id");
        var text = $(e).val();
        clone.find("#" + id + "dest").text(text);
        clone.find("#" + id).remove();
    });

    var range = document.createRange();
    range.selectNode(clone[0]);
    window.getSelection().removeAllRanges(); // clear current selection
    window.getSelection().addRange(range); // to select text
    document.execCommand("copy");
    window.getSelection().removeAllRanges();// to deselect
    clone.empty();
    alert("Šablona je připravená pro vložení do vašeho editoru.");
    return false;
}

$(document).ready(function() {
    $("#modal_select_template").load("resources/templates/modals/select_template.html");

	$.each($("form").find("input[type=radio]"), function (i, e) {
        var id = $(e).attr("id");
        var html = $("#" + id + "val").html();
        if ($(html).length > 0) {
            $("#" + id + "lb").attr("title", html);
        }
    });

    $("input[type=radio]").on("change", function () {
        $.each($("form").find("input[type=radio]"), function (i, e) {
            $(e).prop("checked", false);
            $("#" + $(e).attr("id") + "val").hide();
        });
        $(this).prop("checked", true);
        $("#" + $(this).attr("id") + "val").show();
    });

    $("#date").text(getCurrentDate());
    $('[data-toggle="tooltip"]').tooltip();
    $("#exportDocx").click(function(){
		$('#generatedArea').wordExport();
		return false;
	});
    
	$(".template").prop("contenteditable", "true");
});
